import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import Lost from "./Components/Lost/Lost";
import UserContext, { UserProvider } from "./contexts/UserContext";
import Application from "./Pages/Application/Application";
import Credential from "./Pages/Credential/Credential";
import Home from "./Pages/Home/Home";
import Login from "./Pages/Login/Login";
import Merchant from "./Pages/Merchant/Merchant";
import PanelMerchant from "./Pages/Merchant/PanelMerchant";
import Panier from "./Pages/Panier/Panier";
import SignUp from "./Pages/SignUp/SignUp";
import Transactions from "./Pages/Transactions/Transactions";

const Auth = ({ children }) => {
  const { token } = useContext(UserContext);
  let url = "/login";

  if (window.location.pathname === "/login") {
    url = "/login";
  } else if (window.location.pathname === "/signUp") {
    url = "/signUp";
  }

  return token ? children : <Redirect to={{ pathname: url }} />;
};

const App = () => {
  return (
    <Router>
      <Switch>
        <Route component={Panier} path="/sitemarchand/:idMarchand/panier" />
        <Route
          component={PanelMerchant}
          path="/sitemarchand/:idMarchand/panel"
        />
        <UserProvider>
          <Switch>
            <Route component={Login} path="/login" />
            <Route component={SignUp} path="/signUp" />
            <Auth>
              <Switch>
                <Route exact component={Layout} path="/" />
                <Layout>
                  <Switch>
                    <Route
                      component={Transactions}
                      exact
                      path="/list/transactions"
                    />
                            <Route component={Panier} path="/sitemarchand/:idMarchand/panier" />

                    <Route component={Merchant} exact path="/list/marchands" />
                    <Route component={Application} path="/list/candidatures" />
                    <Route component={Credential} path="/credentials" />
                    <Route component={Home} path="/tableau-de-bord" />
                    <Route component={Panier} path="/panier" />
                  </Switch>
                </Layout>
              </Switch>
            </Auth>
            <Route component={Lost} path="*" />
          </Switch>
        </UserProvider>
      </Switch>
    </Router>
  );
};

export default App;

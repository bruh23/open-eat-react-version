import { Spin } from "antd";
import React from "react";
import styles from "./Loading.module.scss";


// To custom it go check ant design doc => Spin...
const Loading = () => <Spin className={styles.loading} />;

export default Loading;

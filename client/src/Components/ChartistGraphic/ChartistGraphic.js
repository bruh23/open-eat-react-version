import React from 'react'

import ChartistGraph from 'react-chartist';
import LineChart from "react-chartjs"

const ChartistGraphic = () => {


    var data = {
        labels: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        datasets: [
            {
                label: "Transactions annulées",
                fillColor: "#ff9c6e",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40, 76, 25, 60,85,12]
            },
            {
                label: "Transactions Validées",
                fillColor: "#003a8c",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: [28, 48, 40, 19, 86, 27, 90,45,82,14,63,79,24]
            }
        ]
    };

    var options = {
        high: 10,
        low: -10,
        axisX: {
            labelInterpolationFnc: function (value, index) {
                return index % 2 === 0 ? value : null;
            }
        },
     

    };

    var type = 'Bar'


    return (

        <>
        <h1>Taux de transactions</h1>
            <LineChart.Bar data={data} options={options} type={type}   width={800}height={300} />

        </>


    )
}

export default ChartistGraphic


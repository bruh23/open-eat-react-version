import { Button, Result } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import styles from "./Lost.module.scss";

const Lost = () => (
  <Result
    className={styles.lost}
    status="404"
    title="404"
    subTitle="Désolé, il semblerait que vous vous êtes perdus."
    extra={
      <Button type="primary">
        <Link to="/">Retour à l'accueil</Link>
      </Button>
    }
  />
);

export default Lost;

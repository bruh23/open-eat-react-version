import {
  DesktopOutlined,
  DollarCircleOutlined,
  EyeOutlined,
  LogoutOutlined,
  PieChartOutlined,
  SecurityScanOutlined,
  UnorderedListOutlined
} from "@ant-design/icons";
import { Breadcrumb, Layout as Wrapper, Menu } from "antd";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import UserContext from "../../contexts/UserContext";
import styles from "./Layout.module.scss";

const { Header, Content, Sider, Footer } = Wrapper;

const Logo = () => {
  return (
    <div className={styles.logo}>
      <Link to="/"></Link>
    </div>
  );
};
const Layout = ({ children }) => {
  const [collapsed, setCollapse] = useState(false);
  const [breadCrumb, setBreadCrumb] = useState(["Tableau de bord"]);
  const [selectedKey, setSelectedKey] = useState("1");
  const { disconnect, token, decodedToken, selected_merchant } = useContext(
    UserContext
  );
  let role = decodedToken.admin ? "ADMIN" : "MARCHAND";

  const logout = () => {
    console.log("logout user");
    disconnect();
  };

  useEffect(() => {
    let pathname = menu.filter((m) => m.to === window.location.pathname);
    if (pathname.length > 0) {
      setBreadCrumb([`${pathname[0].title}`]);
      setSelectedKey(pathname[0].key);
    }
  }, []);

  const menu = [
    {
      key: "4",
      icon: <EyeOutlined />,
      title: "Marchand",
      onlyFor: "ADMIN",
      type: "select_merchant",
    },
    {
      key: "1",
      icon: <PieChartOutlined />,
      title: "Tableau de bord",
      type: "link",
      to: "/tableau-de-bord",
      fc: () => setBreadCrumb(["Tableau de bord"]),
    },
    {
      key: "2",
      icon: <DesktopOutlined />,
      title: "Liste des marchands",
      type: "link",
      to: "/list/marchands",
      onlyFor: "ADMIN",
      fc: () => setBreadCrumb(["Liste des marchands"]),
    },
    {
      key: "3",
      icon: <DollarCircleOutlined />,
      title: "Liste des transactions",
      type: "link",
      to: "/list/transactions",
      fc: () => setBreadCrumb(["Liste des transactions"]),
    },
    {
      key: "6",
      icon: <UnorderedListOutlined />,
      title: "Candidatures",
      type: "link",
      to: "/list/candidatures",
      onlyFor: "ADMIN",
      fc: () => setBreadCrumb(["Candidatures"]),
    },
    {
      key: "7",
      icon: <SecurityScanOutlined />,
      title: "Credentials",
      type: "link",
      to: "/credentials",
      onlyFor: "MARCHAND",
      fc: () => setBreadCrumb(["Credentials"]),
    },
    {
      key: "8",
      icon: <LogoutOutlined />,
      title: "Se déconnecter",
      type: "link",
      to: "/login",
      fc: () => logout(),
    },
  ];

  if (selected_merchant) {
    console.log("ya kelke chose", selected_merchant.last_name);
  } else {
    console.log("ya r ", selected_merchant);
  }

  console.log("k", role);
  return (
    <Wrapper className={styles.layout}>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={() => setCollapse(!collapsed)}
        breakpoint="lg"
      >
        <Logo />
        <Menu
          theme="dark"
          onClick={(e) => setSelectedKey(e.key)}
          selectedKeys={selectedKey}
          mode="inline"
        >
          {menu.map((m) =>
            m.onlyFor ? (
              m.onlyFor === role && (
                <Menu.Item key={m.key} icon={m.icon} onClick={m.fc}>
                  {m.type === "link" && <Link to={m.to}>{m.title}</Link>}
                  {m.type === "item" && m.title}
                </Menu.Item>
              )
            ) : (
              <Menu.Item key={m.key} icon={m.icon} onClick={m.fc}>
                {m.type === "link" && <Link to={m.to}>{m.title}</Link>}
                {m.type === "item" && m.title}
                {m.type === "select_merchant" && selected_merchant
                  ? selected_merchant.last_name +
                    " " +
                    selected_merchant.first_name
                  : m.title}
              </Menu.Item>
            )
          )}
        </Menu>
      </Sider>
      <Wrapper className={styles.content}>
        <Header style={{ padding: 0 }} />

        <Content style={{ margin: "0 16px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            {breadCrumb.map((b, index) => (
              <Breadcrumb.Item key={index}>{b}</Breadcrumb.Item>
            ))}
          </Breadcrumb>
          <div
            className={styles.content}
            style={{ padding: 24, minHeight: 360 }}
          >
            {children}
          </div>
        </Content>
      </Wrapper>
    </Wrapper>
  );
};
export default Layout;

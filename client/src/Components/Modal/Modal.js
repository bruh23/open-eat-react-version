import React, { useState } from 'react'
import { Modal } from 'antd';
import { DeleteOutlined } from "@ant-design/icons";


const MyModal = ({ title, textModal, record, handleDelete }) => {


    const [visible, setVisible] = useState(false);

 
    const handleConfirm = (key) => {

        handleDelete(key);
        setVisible(false)

    };

    const key = title === "Confirmation de suppression de Credentials !" ? record : record.key
    return (
        <div>
            <DeleteOutlined onClick={() => setVisible(true)} style={{ color: "red", fontSize: "23px" }} />

            <Modal
                title={title}
                visible={visible}
                onOk={() => handleConfirm(key)}
                onCancel={() => setVisible(false)}
                //okButtonProps={{ style: { background:'#001529' }}}

            >
                <p>{record.key}</p>
                <p>{textModal}</p>

            </Modal>
        </div>
    )
}

export default MyModal

import React, {useState} from 'react';
import { Upload, message, Button } from 'antd';
import { UploadOutlined } from '@ant-design/icons';


const UploadFile = () => {


const [kbisBool, setKbisBool] = useState(true)
    const handleUploadKbis = (info) => {
        console.log( `L'information est: ${info}`)
    }

    
    const propsUploadKbis = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            setKbisBool(false)
            console.log(info.file)
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                console.log(info)
                message.success(`${info.file.name} a été téléchargé avec succès`);
            } else if (info.file.status === 'error') {
                message.error(`Error! le téléchargment de ${info.file.name} a été échoué.`);
            }
        },
    };


    const displaybtn = kbisBool ?  <Button > <UploadOutlined /> KBIS</Button> : <Button disabled> <UploadOutlined /> KBIS</Button>

    return (
        <Upload accept={".pdf"} 
        {...propsUploadKbis}  >
            {displaybtn}
        </Upload>
    )
}

export default UploadFile

import jwtDecode from "jwt-decode";
import localforage from "localforage";
import React, { createContext, useEffect, useReducer, useState } from "react";
import Login from "../Pages/Login/Login";
import SignUp from "../Pages/SignUp/SignUp";
import { getMerchant, getMerchants } from "./actions/merchants/merchant";
import { setOperation } from "./actions/operations/operations";
import {
  getAllTransactions,
  getTransactions
} from "./actions/transactions/transactions";
import {
  getMerchantByUserId,
  getUsers,
  login,
  updateUser
} from "./actions/Users/Users";

const UserContext = createContext({ token: "" });

const reducer = (state, action) => {
  switch (action.type) {
    case "SET_TOKEN":
      return {
        ...state,
        token: action.payload,
      };
    case "SET_TRANSACTIONS_MERCHANT":
      return {
        ...state,
        transactions: action.payload,
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: action.payload,
      };
    case "SET_MERCHANT":
      return {
        ...state,
        merchant: action.payload,
      };
    case "LOGOUT":
      return {
        ...state,
        token: "",
      };
    case "SET_MERCHANTS":
      console.log(action.payload);
      return {
        ...state,
        merchants: action.payload,
      };
    case "SET_USERS":
      return {
        ...state,
        users: action.payload,
      };
    case "SET_ADMIN_MERCHANT":
      return {
        ...state,
        selected_merchant: action.payload,
      };
    default:
      return state;
  }
};

export const UserProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, {
    token: "",
    merchant: "",
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    localforage.getItem("token").then((response) => {
      //console.log(response);
      if (response) {
        if (response.token) {
          //console.log("on est entré");
          dispatch({ type: "SET_TOKEN", payload: response.token });
          let decodedToken = jwtDecode(response.token);

          if (!decodedToken.admin) {
            getMerchantByUserId(response.token, decodedToken.id).then(
              (response) => {
                dispatch({ type: "SET_MERCHANT", payload: response });
              }
            );
          } else {
            dispatch({ type: "SET_MERCHANT", payload: "isAdmin" });
          }
        }
      } else {
        dispatch({ type: "SET_TOKEN", payload: "" });
      }
    });
    setLoading(false);
  }, []);

  const isTokenExpired = (response) => {
    if (response.token) {
      if (response.token.length > 0) {
        if (response.token[0].message === "jwt expired") return true;
      }
    }

    return false;
  };

  const value = {
    token: state.token,
    decodedToken: state.token ? jwtDecode(state.token) : "no token",
    signIn: (email, password) => {
      return login(email, password)
        .then((response) => {
          if (response.token) {
            localforage.setItem("token", response);
            dispatch({ type: "SET_TOKEN", payload: response.token });
          }
          return response;
        })
        .catch((error) => {
          console.log(error);
        });
    },
    getMercTrans: (merchant_id) => {
      if (state.token && merchant_id) {
        return getTransactions(state.token, merchant_id).then((response) => {
          if (!isTokenExpired(response)) {
            dispatch({ type: "SET_TRANSACTIONS_MERCHANT", payload: response });
          } else {
            value.disconnect();
          }
        });
      } else if (state.token && value.decodedToken.admin) {
        if (value.selected_merchant) {
          return getTransactions(state.token, value.selected_merchant.id).then(
            (response) => {
              if (!isTokenExpired(response)) {
                dispatch({
                  type: "SET_TRANSACTIONS_MERCHANT",
                  payload: response,
                });
              } else {
                value.disconnect();
              }
            }
          );
        } else {
          return getAllTransactions(state.token).then((response) => {
            if (!isTokenExpired(response)) {
              dispatch({
                type: "SET_TRANSACTIONS_MERCHANT",
                payload: response,
              });
            } else {
              value.disconnect();
            }
          });
        }
      }
    },
    transactions: state.transactions,
    getMerchantByUserId: (token) => {
      console.log(token);
      return getMerchantByUserId(token, jwtDecode(token).id).then(
        (response) => {
          if (!jwtDecode(token).admin)
            dispatch({ type: "SET_MERCHANT", payload: response });
          else dispatch({ type: "SET_MERCHANT", payload: "isAdmin" });
        }
      );
    },
    merchant: state.merchant,
    disconnect: () => {
      localforage.removeItem("token").then(() => {
        dispatch({ type: "LOGOUT" });
      });
    },
    setOperationsTrans: (operation) => {
      if (state.token && operation.id) {
        return setOperation(state.token, operation).then((response) => {
          if (!isTokenExpired(response)) {
            console.log(response);
            // dispatch({type: "SET_OPERATIONS"})
          } else {
            value.disconnect();
          }
        });
      }
    },
    getMerchs: () => {
      return getMerchants(state.token).then((response) => {
        if (!isTokenExpired(response)) {
          dispatch({ type: "SET_MERCHANTS", payload: response });
        } else value.disconnect();
      });
    },
    merchants: state.merchants,
    updtUser: (newUser) => {
      return updateUser(state.token, newUser).then((response) => {
        if (isTokenExpired(response)) value.disconnect();
      });
    },
    getUsers: () => {
      return getUsers(state.token).then((response) => {
        if (!isTokenExpired(response)) {
          dispatch({ type: "SET_USERS", payload: response });
        } else value.disconnect();
      });
    },
    users: state.users,
    onSelectMerc: (merchant_id) => {
      return getMerchant(state.token, merchant_id).then((response) => {
        if (!isTokenExpired(response)) {
          dispatch({ type: "SET_ADMIN_MERCHANT", payload: response });
        } else {
          value.disconnect();
        }
      });
    },
    selected_merchant: state.selected_merchant,
  };

  console.log(value.selected_merchant);
  return (
    <UserContext.Provider value={value}>
      {/* {loading && <Loading />} */}
      {/* {!loading && children} */}
      {value.token && value.merchant ? (
        children
      ) : window.location.pathname === "/login" ? (
        <Login />
      ) : window.location.pathname === "/signUp" ? (
        <SignUp />
      ) : (
        <Login />
      )}
    </UserContext.Provider>
  );
};

export default UserContext;

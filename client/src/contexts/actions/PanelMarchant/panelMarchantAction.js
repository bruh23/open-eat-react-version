export const getProducts = (id, token) =>
  fetch(`http://localhost:3003/products/merchant/${id}`, {
    method: "get",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
}).then((res) => res.json());

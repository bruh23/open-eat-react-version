export const setUser = (merchant) =>
  fetch(`http://localhost:3003/user`, {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
    },
    body: JSON.stringify({ email: merchant.email, password: merchant.password }),
  }).then((res) => res.json());




export const setMerchant = (id, dataMarchant) =>
  fetch(`http://localhost:3003/merchant/${id}`, {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
    },
    body: JSON.stringify(
        {
            first_name : dataMarchant.nom,
            last_name: dataMarchant.prenom,
            name_society: dataMarchant.companyName,
            KBIS: "QZD51QZD51QZD21",
            email: dataMarchant.email,
            phone_number: dataMarchant.phone,
            address: "21 rue nodejs",
            postal_code: "75000",
            city: "Paris",
            currency: dataMarchant.devise,
            valid: true,
            delai_livraison : "50"
        }
    ),
  }).then((res) => res.json());
export const login = (email, password) =>
  fetch(`http://localhost:3003/user/login`, {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
    },
    body: JSON.stringify({ email: email, password: password }),
  }).then((res) => res.json());

export const getUsers = (token) =>
  fetch("http://localhost:3003/user", {
    method: "GET",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

  export const getMerchantByUserId = (token, user_id) =>
  fetch(`http://localhost:3003/user/merchant/${user_id}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

  
  export const updateUser = (token, newUser) =>
  fetch(`http://localhost:3003/user/${newUser.id}`, {
    method: "PATCH",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());
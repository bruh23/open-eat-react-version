export const setOperation = (token, operation) =>
  fetch(`http://localhost:3003/operations/${operation.id}`, {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({ statut: operation.status }),
  }).then((res) => res.json());

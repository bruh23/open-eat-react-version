export const getMerchants = (token) =>
  fetch(`http://localhost:3003/merchant`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

export const getMerchant = (token, merchant_id) =>
  fetch(`http://localhost:3003/merchant/${merchant_id}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

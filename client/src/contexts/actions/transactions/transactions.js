export const getTransactions = (token, merchant_id) =>
  fetch(`http://localhost:3003/transaction/merchant/${merchant_id}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

export const getTransaction = (token, transaction_id) =>
  fetch(`http://localhost:3003/transaction/${transaction_id}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

export const editTransaction = (token, transaction_id) =>
  fetch(`http://localhost:3003/transaction/${transaction_id}`, {
    method: "PATCH",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

export const deleteTransaction = (token, transaction_id) =>
  fetch(`http://localhost:3003/transaction/${transaction_id}`, {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

export const addTransaction = (token, merchant_id) =>
  fetch(`http://localhost:3003/transaction/${merchant_id}`, {
    method: "PUT",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

export const getAllTransactions = (token) =>
  fetch(`http://localhost:3003/transaction`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());

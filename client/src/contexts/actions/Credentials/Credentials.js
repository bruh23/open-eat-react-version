export const getCredentials = (id, token) =>
  fetch(`http://localhost:3003/credentials/${id}`, {
    method: "get",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
}).then((res) => res.json());


export const generateCredentials = (id, token) =>
  fetch(`http://localhost:3003/credentials/${id}`, {
    method: "PATCH",
    headers: {
      "content-type": "application/json",
      accept: "application/json",
      authorization: `Bearer ${token}`,
    },
}).then((res) => res.json());

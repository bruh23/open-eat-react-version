import { Line } from '@ant-design/charts';
import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';
import { Card, Col, Progress, Row, Statistic } from 'antd';
import React, { useState } from "react";
import ChartistGraphic from '../../Components/ChartistGraphic/ChartistGraphic';

const Home = () => {

  const intialData = [
    { mois: 'Janvier', moyenne: 12 },
    { mois: 'Fevrier', moyenne: 4 },
    { mois: 'Mars', moyenne: 3.5 },
    { mois: 'Avril', moyenne: 5 },
    { mois: 'Mai', moyenne: 4.9 },
    { mois: 'Juin', moyenne: 6 },
    { mois: 'Juillet', moyenne: 7 },
    { mois: 'Aout', moyenne: 9 },
    { mois: 'Septembre', moyenne: 13 },
    { mois: 'Octobre', moyenne: 7 },
    { mois: 'Novembre', moyenne: 10 },
    { mois: 'Décembre', moyenne: 8 },
  ];

  const stat = {
    validateApplicationStat: 45,
    turnover: 1248.24,
    nbMarchands: 426,
    nbTransactions: 45792

  }
  const [data, setData] = useState(intialData);
  const [validateApplicationStat, setValidateApplicationStat] = useState(stat.validateApplicationStat);
  const [turnover, setTurnover] = useState(stat.turnover);
  const [nbMarchands, setNbMarchands] = useState(stat.nbMarchands);
  const [nbTransactions, setNbTransactions] = useState(stat.nbTransactions);


  console.log(intialData)
  console.log(data)
  const config = {
    data,
    title: {
      visible: true,
      text: 'Taux de transactions par mois',
    },
    xField: 'mois',
    yField: 'moyenne',
  };




  return (
    <>

      <Row style={{ marginBottom: 50 }} type="flex" justify="space-between">
        <Col span={4}>
          <ChartistGraphic />
        </Col>

        <Col span={4}>
          <Card style={{ borderColor: "white" , background: "#fafafa"}}>
            <Statistic
              title="Chiffre d'affaire"
              value={turnover}
              precision={2}
              valueStyle={{ color: '#3f8600' }}
              prefix={<ArrowUpOutlined />}
              suffix="€"
            />
          </Card>
        </Col>



      </Row>

      <Card style={{ marginBottom: 50, background: "#feffe6" }}>
        <Row type="flex" justify="space-between">
          <Col span={4}>

            <Statistic title="Nombre de marchants" value={nbMarchands} />

          </Col>
          <Col span={4}>

            <Statistic title="Nombre de transactions" value={nbTransactions} />

          </Col>
          <Col span={4}>

            <Statistic title="Chiffre d'affaire" value={turnover} />

          </Col>
          <Col span={4}>
            <Card style={{ borderColor: "#feffe6", background: "#f4ffb8" }}>
              <Statistic
                title="Chiffre d'affaire"
                value={turnover}
                precision={2}
                valueStyle={{ color: '#cf1322' }}
                prefix={<ArrowDownOutlined />}
                suffix="€"
              />
            </Card>
          </Col>
        </Row>

      </Card>

      <Row style={{ marginBottom: 50 }} type="flex" justify="space-around">
        <Col span={4}>
          <Progress type="circle" percent={validateApplicationStat} width={140} strokeColor={{ '0%': '#391085', '100%': '#ffa39e' }} />
          <h4>Candidature acceptées</h4>

        </Col>

        <Col span={4}>
          <Progress type="circle" percent={90} width={140} strokeColor={{ '0%': '#ad4e00', '100%': '#ffec3d', }} />
          <h4>Candidature acceptées</h4>

        </Col>
        <Col span={4}>
          <Progress type="circle" percent={80} width={140} strokeColor={{ '0%': '#108ee9', '100%': '#87d068', }} />
          <h4>Candidature acceptées</h4>

        </Col>


      </Row>


      <Line {...config} />


    </>
  );
};

export default Home;

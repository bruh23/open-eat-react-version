import {
    CheckCircleOutlined,
    DownloadOutlined,
    SearchOutlined
} from "@ant-design/icons";
import { Button, Input, notification, Space, Table, Typography } from "antd";
import React, {
    useContext,
    useEffect,
    useReducer,
    useRef,
    useState
} from "react";
import Highlighter from "react-highlight-words";
import { Link } from "react-router-dom";
import UserContext from "../../contexts/UserContext";
import Pdf from "../../Pdf/zztestPdf.pdf";

const initialData = [
  {
    key: 1,
    name: "Raouf loucif",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
    state: "new application",
  },
  {
    key: 2,
    name: "John Brown",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
  },
  {
    key: 3,
    name: "John Brown",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
  },
  {
    key: 4,
    name: "John Brown",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
  },
  {
    key: 5,
    name: "John Brown",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
  },
  {
    key: 6,
    name: "John Brown",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
  },
  {
    key: 7,
    name: "John Brown",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
  },
  {
    key: 8,
    name: "John Brown",
    Entreprise: "Agence de voyage",
    tel: "+33 64781154",
    email: "platforme@gmail.com",
    address: "242 Rue du Faubourg Saint-Antoine, 75012 Paris",
    kbis: (
      <Link to={Pdf} target="_blank">
        <DownloadOutlined style={{ color: "#5ad4f4", fontSize: "23px" }} />{" "}
      </Link>
    ),
    date: "02/07/2020 16:41:14",
  },
];

const openValidationNotification = (placement, name, email) => {
  notification.info({
    message: (
      <Typography.Text strong> Validation du compte de {name}</Typography.Text>
    ),
    description: `Le compte marchant a été validé. Une notification vient d'être envoyée à l'adresse ${email}`,
    placement,
    icon: <CheckCircleOutlined style={{ color: "#5b8c00" }} />,
  });
};

const openRefusalNotification = (placement, name, email) => {
  notification.info({
    message: (
      <Typography.Text strong> Candidature refusée {name}</Typography.Text>
    ),
    description: `La candidture a été refusée. Une notification vient d\'être envoyée ${email}`,
    placement,
    icon: <CheckCircleOutlined style={{ color: "#cf1322" }} />,
  });
};

function reducer(state, action) {
  switch (action.type) {
    case "validationApplication": {
      openValidationNotification("topRight", action.name, action.email);
      return state.filter((item) => item.key !== action.key);
    }
    case "refusalApplication": {
      openRefusalNotification("topRight", action.name, action.email);
      return state.filter((item) => item.key !== action.key);
    }
    case "reinitialiser":
      return initialData;
    default:
      return state;
  }
}
const Application = () => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const node = useRef();
  const [marchands, dispatch] = useReducer(reducer, initialData);
  const { updtUser, users, getUsers } = useContext(UserContext);

  useEffect(() => {
    getUsers();
  }, []);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node}
          placeholder={`Rechercher ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 95 }}
          >
            Lancer
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Réinitialiser
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        //console.log(setSearchInput)
        //setTimeout(() => setSearchInput.select());
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "user_id",
      key: "user_id",
      //width: '30%',
      ...getColumnSearchProps("user_id"),
    },

    {
      title: "Type",
      dataIndex: "type",
      key: "type",
      //width: '30%',
      ...getColumnSearchProps("type"),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      //width: '30%',
      ...getColumnSearchProps("email"),
    },
    {
      title: "",
      dataIndex: "operation",
      render: (text, record) =>
        initialData.length >= 1 ? (
          <>
            {/*                         <MyModal handleDelete={handleDelete} title="confirmation de suppression" textModal={textModal} record={record} />
             */}
            <CheckCircleOutlined
              onClick={() => {
                dispatch({
                  type: "validationApplication",
                  key: record.key,
                  name: record.name,
                  email: record.email,
                });
                console.log(record);
                updtUser({ id: record.merchant_id, confirm: true });
              }}
              style={{ color: "green", fontSize: "23px" }}
            />
            {/* <CloseCircleOutlined
              onClick={() => {
                dispatch({
                  type: "refusalApplication",
                  key: record.key,
                  key: record.key,
                  name: record.name,
                  email: record.email,
                });
                updtUser({ id: record.merchant_id, confirm: false });
              }}
              style={{ color: "red", fontSize: "23px" }}
            /> */}
          </>
        ) : null,
    },
  ];

  const [columnsMarchads, setColumnsMarchads] = useState(columns);

  console.log(users);
  return (
    <>
      <Typography.Title level={3}>Les candidatures</Typography.Title>
      {users && (
        <Table
          columns={columnsMarchads}
          dataSource={users
            .filter((m) => !m.confirm)
            .map((t, index) => {
              return {
                key: index,
                merchant_id: t.userId,
                firstname: t.first_name,
                lastname: t.last_name,
                kbis: t.KBIS,
                tel: t.phone_number,
                email: t.email,
                currency: t.currency,
              };
            })}
        />
      )}
    </>
  );
};

export default Application;

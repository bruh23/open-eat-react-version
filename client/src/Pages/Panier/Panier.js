import { Button, Card, Col, Row, Space, Table } from "antd";
import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loading from "../../Components/Loading/Loading";
import { setOperation } from "../../contexts/actions/operations/operations";
import { getProducts } from "../../contexts/actions/PanelMarchant/panelMarchantAction";
import { addTransaction } from "../../contexts/actions/transactions/transactions";
import UserContext from "../../contexts/UserContext";
import produit from "./thisrestaurants.mockup.json";

const { Meta } = Card;

const Panier = (props) => {
  const { token, merchant } = useContext(UserContext);

  console.log(props.location);

  const [produits, setProduits] = useState([]);
  const [loading, setLoading] = useState(false);
  const [quantity, setQuantity] = useState();
  const [transaction, setTransaction] = useState(false);
  const { idMarchand } = useParams();

  const columns = [
    {
      title: "Produit",
      dataIndex: "nom_product",
      key: "nom_product",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Quantité",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            type="primary"
            disabled={quantity === 0}
            onClick={() => setQuantity((record.quantity = record.quantity - 1))}
          >
            -
          </Button>

          <Button
            type="primary"
            onClick={() => {
              setQuantity((record.quantity = record.quantity + 1));
            }}
          >
            +
          </Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    console.log(idMarchand);
    console.log(produit);
    getProducts(idMarchand)
      .then((response) => {
        console.log(response);
        setProduits(response);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleCommande = (key) => {
    console.log(addTransaction);

    return addTransaction("", idMarchand)
      .then((response) => {
        console.log(response);
        setOperation("", response);
        setTransaction(response);
        return response
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Row type="flex">
        <Col xs={24} sm={24} md={24} lg={24} xl={5}>
          <br />
          <Card
            style={{ width: 300, height: 450 }}
            cover={
              <img
                alt="restaurant"
                src={`https://source.unsplash.com/300x300/?burger`}
              />
            }
          >
            <Meta title="Informations" />
            {/* <Restaurant /> */}
          </Card>
        </Col>
        <Col xs={24} sm={24} md={24} lg={24} xl={19}>
          <br />
          {loading ? (
            <Loading />
          ) : (
            <Table columns={columns} dataSource={produits} />
          )}
          <Button
            type="primary"
            block
            onClick={async () => {
              let x = await handleCommande();
              console.log(x);
              window.location.href = `http://localhost:3003/transaction/show/${x.id}`;
            }}
          >
            Voir la commande
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default Panier;

import React, { useState,  } from 'react';
import { Link, useHistory } from "react-router-dom";
import { Form, Input, Typography, Select, Checkbox, Button, AutoComplete } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import styles from "../Login/Login.module.scss"
import classNames from "classnames";
import UploadFile from "../../Components/UploadFile/UploadFile";
import { setUser, setMerchant } from "../../contexts/actions/Marchands/Marchant";



const { Option } = Select;
const AutoCompleteOption = AutoComplete.Option;
const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 10,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

const SignUp = (props) => {

    let history = useHistory()
    const data = {
        id: null,
        nom: '',
        prenom: '',
        companyName: '',
        email: '',
        phone: '',
        devise: 'Euro',
        redirecteUrl: '',
        redirecteUrlValue: '',
        annulationUrl: '',
        annulationUrlValue: '',
        kbis: '',
        password: '',
        urlConfirmationBefore: 'http://',
        urlConfirmationAfter: '.com',
        urlAnnulationBefore: 'http://',
        urlAnnulationAfter: '.com',

    }
    const [dataMarchad, setDataMarchad] = useState(data);
    const { id, nom, prenom, companyName, email, phone, devise, redirecteUrl,
        annulationUrl, password, urlAnnulationBefore, urlAnnulationAfter, urlConfirmationBefore,
        urlConfirmationAfter, redirecteUrlValue, annulationUrlValue } = dataMarchad;

    const selectBeforeAnnulatin = (
        <Select defaultValue="http://" className="select-before" value={urlAnnulationBefore}
            onChange={value => handleUrl(value, 'urlAnnulationBefore')} >
            <Option value="http://">http://</Option>
            <Option value="https://">https://</Option>
        </Select>
    );
    const selectAfterAnnulation = (
        <Select defaultValue=".com" className="select-after" value={urlAnnulationAfter}
            onChange={value => handleUrl(value, 'urlAnnulationAfter')} >
            <Option value=".com">.com</Option>
            <Option value=".fr">.fr</Option>
            <Option value=".cn">.cn</Option>
            <Option value=".org">.org</Option>
        </Select>
    );

    const selectBeforeConfirmation = (
        <Select defaultValue="http://" className="select-before" value={urlConfirmationBefore}
            onChange={value => handleUrl(value, 'urlConfirmationBefore')} >
            <Option value="http://">http://</Option>
            <Option value="https://">https://</Option>
        </Select>
    );
    const selectAfterConfirmation = (
        <Select defaultValue=".com" className="select-after" value={urlConfirmationAfter}
            onChange={value => handleUrl(value, 'urlConfirmationAfter')} >
            <Option value=".com">.com</Option>
            <Option value=".fr">.fr</Option>
            <Option value=".cn">.cn</Option>
            <Option value=".org">.org</Option>
        </Select>
    );

    const handleUrl = (value, type) => {

        let before = "";
        let after = "";
        let name = "";
        let urlValue = "";

        switch (type) {
            case 'urlConfirmationBefore':
                name = "redirecteUrl";
                urlValue = redirecteUrlValue;
                before = value;
                after = urlConfirmationAfter
                break;
            case 'urlConfirmationAfter':
                name = "redirecteUrl";
                urlValue = redirecteUrlValue;
                before = urlConfirmationBefore;
                after = value
                break;
            case 'urlAnnulationBefore':
                name = "annulationUrl";
                urlValue = annulationUrlValue;
                before = value;
                after = urlAnnulationAfter
                break;
            case 'urlAnnulationAfter':
                name = "annulationUrl";
                urlValue = annulationUrlValue;
                before = urlAnnulationBefore;
                after = value
                break;

            default:
                break;
        }


        setDataMarchad({
            ...dataMarchad,
            [type]: value,
            [name]: before + urlValue + after

        });


    }
    const handleChange = e => {

        if (e.target.name === 'redirecteUrlValue') {
            setDataMarchad({
                ...dataMarchad, ['redirecteUrl']:
                    urlConfirmationBefore + e.target.value + urlConfirmationAfter,
                ['redirecteUrlValue']: e.target.value
            });

        }
        else if (e.target.name === 'annulationUrlValue') {
            setDataMarchad({
                ...dataMarchad, ['annulationUrl']:
                    urlAnnulationBefore + e.target.value + urlAnnulationAfter,
                ['annulationUrlValue']: e.target.value
            });
        }
        else {
            setDataMarchad({ ...dataMarchad, [e.target.name]: e.target.value })

        }
    }

    const [form] = Form.useForm();


    const handleMarchand = event => {

        
        //event.preventDefault();
        let id = null
        setUser(dataMarchad)
            .then((user) => {
                console.log(user)

                id = user.id

                return user;
            })
            .then((merchant) => {
                setMerchant(id, dataMarchad)
                    .then((merchant) => {
                        console.log(merchant)

                        return merchant;
                    })
                    .catch((error) => {
                        console.log(error);
                    });

                return merchant;
            })
            .then(() => history.push({pathname:"/login"})
            )
            .catch(error => {
                console.log("j'ai une erreur");

                console.log(error);
            });


    };

    const showError = errorInfo => {

        console.log(errorInfo)


    }
    return (

        <>

            <Form
                style={{ height: '100%' }}                {...formItemLayout}
                form={form}

                className={classNames(styles.login, "login-form")}
                name="signUp"
                onFinish={handleMarchand}
                onFinishFailed={showError}
                scrollToFirstError
            >
                <Typography.Title level={3}>S'inscrire</Typography.Title >

                <Form.Item
                    name="nom"
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez insérer votre nom',
                            whitespace: true,
                        },
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}

                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Nom"
                        onChange={handleChange}
                        name="nom"
                    />
                </Form.Item>


                <Form.Item
                    name="prenom"
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez insérer votre prénom',
                            whitespace: true,
                        },
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}

                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Prénom"
                        name="prenom"
                        onChange={handleChange} />
                </Form.Item>

                <Form.Item
                    name="companyName"
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez insérer le nom d\'entreprise',
                            whitespace: true,
                        },
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}

                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Nom de société'"
                        value={companyName}
                        name="companyName"
                        onChange={handleChange} />
                </Form.Item>
                <Form.Item
                    name="email"
                    rules={[
                        {
                            type: 'email',
                            message: 'Email n\'est pas valide!',
                        },
                        {
                            required: true,
                            message: 'Veuillez mettre un email correcte',
                        },
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}

                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="Email"
                        value={email}
                        name="email"
                        onChange={handleChange}
                    />
                </Form.Item>


                <Form.Item
                    name="phone"
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez mettre votre numéro de téléphone!',
                        },
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}

                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        placeholder="téléphone"
                        value={phone}
                        name="phone"
                        onChange={handleChange}
                    />
                </Form.Item>

                <Form.Item
                    name='devise'
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}
                    name='devise'

                >
                    <Select defaultValue="Euro" onChange={handleChange}
                        onChange={value => setDataMarchad({ ...dataMarchad, ['devise']: value })}

                        name='devise'
                        value={devise}
                    >
                        <Option value="Euro">Euro (€)</Option>
                        <Option value="Dollar">Dollar ($)</Option>

                    </Select>
                </Form.Item>
                <Form.Item
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}
                >
                    <UploadFile />
                </Form.Item>

                <Form.Item
                    name="redirecteUrlValue"
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez insérer une URL correcte!',
                        },
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}
                >

                    <Input
                        placeholder="Url de redirection "
                        addonBefore={selectBeforeConfirmation} addonAfter={selectAfterConfirmation}
                        value={redirecteUrlValue}
                        name="redirecteUrlValue"
                        onChange={handleChange}

                    />
                </Form.Item>

                <Form.Item
                    name="annulationUrlValue"
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez insérer une URL correcte!',
                        },
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}
                >

                    <Input
                        placeholder=" Url d'annulation "
                        addonBefore={selectBeforeAnnulatin} addonAfter={selectAfterAnnulation}
                        value={annulationUrlValue}
                        name="annulationUrlValue"
                        onChange={handleChange}

                    />


                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez insérer votre mot de passe!',
                        },
                    ]}
                    hasFeedback
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}

                >
                    <Input.Password
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        placeholder="mot de passe"
                        name="password"
                        onChange={handleChange}
                    />
                </Form.Item>

                <Form.Item
                    name="confirm"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Veuillez confirmer votre mot de passe',
                        },
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }

                                return Promise.reject('Les deux mots de passe que vous avez entrés ne correspondent pas!');
                            },
                        }),
                    ]}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}

                >
                    <Input.Password
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        placeholder="confirmation mot de passe"
                        onChange={handleChange}
                    />
                </Form.Item>

                <Form.Item
                    name="agreement"
                    valuePropName="checked"
                    rules={[
                        {
                            validator: (_, value) =>
                                value ? Promise.resolve() : Promise.reject('Veuillez accepter les règles générales '),
                        },
                    ]}
                    {...tailFormItemLayout}
                    wrapperCol={{ sm: 24 }}
                    style={{ width: "30%", marginRight: 0 }}
                >
                    <Checkbox>
                        Oui, j'accepte <a href="">les règles générales</a> de l'API
                </Checkbox>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="success" htmlType="submit"
                        style={{ background: "#096dd9", color: "white" }}
                    >
                        S'inscrire </Button>

                </Form.Item>
                <Link to="/login" style={{ marginBottom: 20 }}> se connecter</Link>,

            </Form>

        </>

    );
};

export default SignUp



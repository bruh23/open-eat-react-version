import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Checkbox, Form, Input } from "antd";
import classNames from "classnames";
import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import UserContext from "../../contexts/UserContext";
import styles from "./Login.module.scss";

const Login = () => {
  const { signIn, getMerchantByUserId } = useContext(UserContext);
  let history = useHistory();
  const [error, setError] = useState(false);

  const onFinish = (values) => {
    console.log(signIn);
    signIn(values.username, values.password).then((response) => {
      if (response.error) {
        setError(response.error);
      } else {
        getMerchantByUserId(response.token);
        history.push({ pathname: "/" });
      }
    });
  };

  return (
    <Form
      name="normal_login"
      className={classNames(styles.login, "login-form")}
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      {error && <p style={{ color: "red" }}>{error}</p>}
      <Form.Item
        name="username"
        rules={[{ required: true, message: "Please input your Username!" }]}
      >
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="Username"
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: "Please input your Password!" }]}
      >
        <Input
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
        />
      </Form.Item>
      <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <a className="login-form-forgot" href="/#">
          Forgot password
        </a>
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Log in
        </Button>
        Or <a href="/#">register now!</a>
      </Form.Item>
    </Form>
  );
};

export default Login;

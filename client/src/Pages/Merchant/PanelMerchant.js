import { Button, Form, Input, Tabs } from "antd";
import React, { useState } from "react";
import Loading from "../../Components/Loading/Loading";
import { DataTransactions } from "../Transactions/Transactions";

const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const PanelMerchant = () => {
  const [clientToken, setClientToken] = useState("");
  const [clientSecret, setClientSecret] = useState("");
  const [loading, setLoading] = useState(false);
  const [transactions, setTransactions] = useState(false);
  const [error, setError] = useState(false);

  const onFinish = (values) => {
    console.log("Success:", values);

    setClientToken(values.client_token);
    setClientSecret(values.client_secret);

    setLoading(true);

    getTransactions(values.client_token, values.client_secret)
      .then((response) => {
        console.log(response);
        setTransactions(response);
        setError(false);
      })
      .catch((error) => {
        console.log(error);
        setError("Vos crédentials semble être incorrect !");
      });
    setLoading(false);
  };

  const getTransactions = (CT, CS) => {
    console.log(clientSecret, clientToken);
    return fetch(`http://localhost:3003/api/transactions`, {
      method: "GET",
      headers: {
        "content-type": "application/json",
        accept: "application/json",
        authorization: `client_token=${CT} client_secret=${CS}`,
      },
    }).then((res) => res.json());
  };

  const setOp = (params) => {
    console.log(clientSecret, clientToken);

    return fetch(`http://localhost:3003/api/operations/${params.id}`, {
      method: "PUT",
      headers: {
        "content-type": "application/json",
        accept: "application/json",
        authorization: `client_token=${clientToken} client_secret=${clientSecret}`,
      },
      body: JSON.stringify({ statut: params.status }),
    }).then((res) => res.json());
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  console.log(transactions);
  return (
    <div>
      {loading && <Loading />}
      {!loading && error && <p style={{ color: "red" }}>{error}</p>}
      {!loading && (
        <>
          Renseigner vos credentials
          <Form
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
            }}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Client Token"
              name="client_token"
              style={{ width: "50%" }}
              rules={[
                { required: true, message: "Please input your Client Token!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Client Secret"
              name="client_secret"
              style={{ width: "50%" }}
              rules={[
                { required: true, message: "Please input your Client Secret!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </>
      )}
      <div>
        {transactions && (
          <DataTransactions
            transactions={transactions}
            setOperationsTrans={setOp}
          />
        )}
      </div>
    </div>
  );
};

export default PanelMerchant;

import { DeleteOutlined, EyeOutlined, SearchOutlined } from "@ant-design/icons";
import { Alert, Button, Input, Space, Table } from "antd";
import {
  default as React,
  useContext,
  useEffect,
  useRef,
  useState
} from "react";
import Highlighter from "react-highlight-words";
import UserContext from "../../contexts/UserContext";

const Merchant = () => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [searchInput, setSearchInput] = useState("");
  //const [marchands, setMarchands] = useState([]);.
  const { getMerchs, merchants, onSelectMerc, selected_merchant } = useContext(
    UserContext
  );

  const [alertDelete, setAlertDelete] = useState(false);
  const node = useRef();

  useEffect(() => {
    getMerchs();
  }, []);

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node}
          placeholder={`Rechercher ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 95 }}
          >
            Lancer
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Réinitialiser
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        //console.log(setSearchInput)
        //setTimeout(() => setSearchInput.select());
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "merchant_id",
      key: "merchant_id",
      //width: '30%',
      ...getColumnSearchProps("merchant_id"),
    },
    {
      title: "Nom",
      dataIndex: "lastname",
      key: "lastname",
      //width: '30%',
      ...getColumnSearchProps("lastname"),
    },
    {
      title: "Prenom",
      dataIndex: "firstname",
      key: "firstname",
      //width: '30%',
      ...getColumnSearchProps("firstname"),
    },
    {
      title: "Tél",
      dataIndex: "tel",
      key: "tel",
      //width: '30%',
      ...getColumnSearchProps("tel"),
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      //width: '30%',
      ...getColumnSearchProps("email"),
    },

    {
      title: "Ville",
      dataIndex: "city",
      key: "city",
      ...getColumnSearchProps("city"),
    },
    {
      title: "Kbis",
      dataIndex: "kbis",
      key: "kbis",
      //width: '30%',
      ...getColumnSearchProps("kbis"),
    },
    {
      title: "Currency",
      dataIndex: "currency",
      key: "currency",
      //width: '30%',
      ...getColumnSearchProps("currency"),
    },
    {
      title: "Confirmation",
      dataIndex: "url_confirmation",
      key: "url_confirmation",
      //width: '30%',
      ...getColumnSearchProps("url_confirmation"),
    },
    {
      title: "Cancellation",
      dataIndex: "url_cancellation",
      key: "url_cancellation",
      //width: '30%',
      ...getColumnSearchProps("url_cancellation"),
    },
    {
      title: "operation",
      dataIndex: "operation",
      render: (text, record) => (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-evenly",
          }}
        >
          <Button
            type="default"
            icon={<EyeOutlined />}
            shape="circle"
            onClick={() => {
              console.log(record);
              onSelectMerc(record.merchant_id);
            }}
          ></Button>
          <Button
            type="default"
            icon={<DeleteOutlined />}
            shape="circle"
          ></Button>
        </div>
      ),
    },
  ];

  const handleDelete = (key) => alert("ok");

  console.log(merchants);
  const [columnsMarchads, setColumnsMarchads] = useState(columns);
  const textModal = "Voulez-vous vraiement supprmier ce marchant ?";

  const displayAlertDelete = alertDelete && (
    <Alert
      message="Le marchant a été bien supprimé"
      type="success"
      showIcon
      closable
    />
  );

  return (
    <>
      {displayAlertDelete}
      <h2>Les marchands</h2>
      {merchants && (
        <Table
          columns={columnsMarchads}
          dataSource={merchants.map((m) => {
            return {
              merchant_id: m.id,
              lastname: m.last_name,
              firstname: m.first_name,
              tel: m.phone_number,
              email: m.email,
              kbis: m.kbis,
              city: m.city,
              currency: m.currency,
              confirmation: m.url_confirmation,
              cancel: m.url_cancellation,
            };
          })}
        />
      )}
    </>
  );
};

export default Merchant;

import { SearchOutlined } from "@ant-design/icons";
import { Button, Form, Input, Space, Table } from "antd";
import Modal from "antd/lib/modal/Modal";
import React, { useContext, useEffect, useRef, useState } from "react";
import UserContext from "../../contexts/UserContext";

export const DataTransactions = ({ transactions, setOperationsTrans }) => {
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const [visible, setVisible] = useState(false);
  const [seletctedTransaction, setSelectedTransaction] = useState(false);
  const searchInput = useRef(null);

  console.log(transactions);

  const onFinish = (values) => {
    console.log("Success:", values);
    setOperationsTrans({
      id: seletctedTransaction.transaction_id,
      status: "remboursement",
    });
    setVisible(false);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current.select());
      }
    },
    render: (text) => (searchedColumn === dataIndex ? text : text),
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const columns = [
    {
      title: "Transaction Num.",
      dataIndex: "transaction_id",
      key: "transaction_id",
      ...getColumnSearchProps("transaction_id"),
    },
    {
      title: "Merchent ID",
      dataIndex: "merchent_iD",
      key: "merchent_iD",
      ...getColumnSearchProps("address"),
    },
    {
      title: "Acheteur",
      dataIndex: "acheteur",
      key: "acheteur",
      width: "30%",
      ...getColumnSearchProps("acheteur"),
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
      width: "20%",
      ...getColumnSearchProps("price"),
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",
      ...getColumnSearchProps("address"),
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            type="default"
            onClick={() => {
              setSelectedTransaction(record);
              setVisible(true);
            }}
          >
            Rembourser
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <>
      {transactions && (
        <>
          <Table
            columns={columns}
            expandable={{
              expandedRowRender: (record) => (
                <ul>
                  {record.operations.map((o) => (
                    <li>{o.statut}</li>
                  ))}
                </ul>
              ),
              rowExpandable: (record) => record.operations.length > 0,
            }}
            dataSource={transactions.map((t, index) => {
              let acheteur = false;
              if (t.acheteurINFO.length > 0) {
                if (JSON.parse(t.acheteurINFO[0])[0])
                  acheteur = JSON.parse(t.acheteurINFO[0])[0];
              }

              return {
                key: index,
                transaction_id: t.id,
                merchent_iD: t.merchantId,
                acheteur: (
                  <div>
                    <ul>
                      {acheteur && (
                        <li>
                          {acheteur.first_name + " " + acheteur.last_name}
                        </li>
                      )}
                    </ul>
                  </div>
                ),
                price: t.price,
                state: t.ended ? "Terminé" : "En cours",
                operations: t.operations,
              };
            })}
          />
          <Modal
            title="Remboursement"
            visible={visible}
            onCancel={() => setVisible(false)}
            footer={[
              <Button key="back" onClick={() => setVisible(false)}>
                Annuler
              </Button>,
            ]}
          >
            <Form
              name="basic"
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              <Form.Item
                label="Montant remboursement"
                name="remboursmement"
                rules={[
                  {
                    required: true,
                    message: "Entrer le montant s'il vous plaît!",
                  },
                ]}
              >
                <Input type="number" min={0} max={seletctedTransaction.price} />
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Modal>
        </>
      )}
    </>
  );
};

const Transactions = () => {
  const {
    getMercTrans,
    transactions,
    merchant,
    setOperationsTrans,
  } = useContext(UserContext);

  useEffect(() => {
    getMercTrans(merchant.id);
  }, []);

  return (
    <DataTransactions
      transactions={transactions}
      setOperations={setOperationsTrans}
    />
  );
};

export default Transactions;

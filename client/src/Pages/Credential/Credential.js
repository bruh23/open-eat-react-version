import React, { useContext, useEffect, useState } from 'react'
import { Card, Typography, notification } from 'antd';
import { CheckCircleOutlined } from '@ant-design/icons';
import MyModal from '../../Components/Modal/Modal';
import { getCredentials } from "../../contexts/actions/Credentials/Credentials";
import { generateCredentials } from "../../contexts/actions/Credentials/Credentials";
import UserContext from '../../contexts/UserContext';


const { Text } = Typography;
const openValidationNotification = placement => {
    notification.info({
        message: <Text strong> Génération Token</Text>,
        description: 'Un nouveau client_token et client_secret sont générés, les anciennes clés ont été supprimées',
        placement,
        icon: <CheckCircleOutlined style={{ color: '#5b8c00' }} />,

    });
};


const tabListCredentials = [
    {
        key: 'client_token',
        tab: 'client_token',
    },
    {
        key: 'client_secret',
        tab: 'client_secret',
    },
];


const data = {

    id: "",
    active: 'client_token',
    client_token: "",
    client_secret: "",
}

const Credential = () => {

    const { token, merchant } = useContext(UserContext)
    const [credentials, setCredentials] = useState(data)


    useEffect(() => {
        //getCredendials(merchant.id, token)

        if (token && merchant.id) {

            getCredentials(merchant.id, token).
                then((response) => {
                    //if (!isTokenExpired(response)) {
                    console.log(response)

                    setCredentials({
                        id: response.id,
                        active: 'client_token',
                        client_token: response.client_token,
                        client_secret: response.client_secret,
                    })
                    //} else {
                    //value.disconnect();
                    //  console.log("error useUeefct")
                    //}
                }).catch((error) => {
                    console.log(error);
                });
        }


    }, []);

    const { id, active, client_token, client_secret } = credentials;
    const handleDelete = key => {

        generateCredentials(merchant.id, token).
            then((response) => {
                if (!isTokenExpired(response)) {
                    console.log(response)

                    setCredentials({
                        id: response[1][0].id,
                        active: 'client_token',
                        client_token: response[1][0].client_token,
                        client_secret: response[1][0].client_secret,
                    })

                    openValidationNotification('topRight');
                }
                else {
                    //value.disconnect();
                    console.log("error handleDelete")
                }
            }).catch((error) => {
                console.log(error);
            });
    }

    const contentListToken = {
        client_token: <Text keyboard level={6}>{client_token}</Text>
        ,
        client_secret: <Text keyboard level={6}>{client_secret}</Text>
    };



    const onChangeToken = (key, type) => setCredentials({ ...credentials, [type]: key })



    const isTokenExpired = (response) => {
        if (response.token) {
            if (response.token.length > 0) {
                if (response.token[0].message === "jwt expired") return true;
            }
        }

        return false;
    };

    


    const textModal = "Voulez-vous vraiement supprmier ces credentials et générer Un nouveau client_token et client_secret ?"


    return (
        <>

            <Typography.Title level={3}>Credentials</Typography.Title >

            <Card
                style={{ width: '100%' }}
                tabList={tabListCredentials}
                activeTabKey={active}
                tabBarExtraContent={<MyModal handleDelete={handleDelete} title="Confirmation de suppression de Credentials !" textModal={textModal} record={merchant.id} />}
                onTabChange={key => {
                    onChangeToken(key, 'active');
                }}
            >
                {contentListToken[active]}
            </Card>
        </>
    )
}



export default Credential;
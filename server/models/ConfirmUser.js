const mongoose = require("mongoose");
const connect = require("../lib/db");

const ConfirmUserSchema = new mongoose.Schema({
    token: String,
    expireDate: Number,
    userId: Number
}, {
    collection: "ConfirmUser"
});

const ConfirmUser = connect.model("ConfirmUser", ConfirmUserSchema);

module.exports = ConfirmUser;
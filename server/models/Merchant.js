const mongoose = require("mongoose");
const connect = require("../lib/db");

const MerchantSchema = new mongoose.Schema({
    id: Number,
    first_name: String,
    last_name: String,
    KBIS: String,
    phone_number: String,
    postal_code: String,
    city: String,
    url_confirmation: String,
    url_cancellation: String,
    currency: String,
    valid: Boolean,
    email: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    },
    credentials: Object,
    products: Array,
    userId: Number
}, {
    collection: "Merchant"
});

const Merchants = connect.model("Merchant", MerchantSchema);

module.exports = Merchants;
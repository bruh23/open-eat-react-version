module.exports = {
    connection: require('../lib/db'),
    User: require('./User'),
    Merchant: require('./Merchant'),
    ConfirmUser: require('./ConfirmUser')
};
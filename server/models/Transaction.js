const mongoose = require("mongoose");
const connect = require("../lib/db");

const TransactionSchema = new mongoose.Schema({
    id: Number,
    price: String,
    ended: Boolean,
    acheteurINFO: Array,
    products: Array,
    operations: Array,
    merchantId: Number
}, {
    collection: "Transaction"
});

const Transaction = connect.model("Transaction", TransactionSchema);

module.exports = Transaction;
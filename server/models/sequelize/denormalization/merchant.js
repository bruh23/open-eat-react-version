const MerchantMongo = require("../../Merchant");
const Merchant      = require('../Merchant');

const denormalizeMerchant = async (merchant, operation) => {
    console.log('ADD MERCHANT');

    merchant = merchant.dataValues;

    await MerchantMongo.deleteOne({ id: merchant.id });
    
    if (operation !== "delete") {
        
        merchant.userId = merchant.UserId;

        const doc = new MerchantMongo(merchant);
        await doc.save();
    }
};

console.log('Add hook Merchant');

Merchant.addHook('afterCreate', (merchant) => {
    denormalizeMerchant(merchant, 'create');
});

Merchant.addHook("afterUpdate", (merchant) => {
    denormalizeMerchant(merchant, "update");
});

Merchant.addHook("afterDestroy", (merchant) => {
    denormalizeMerchant(merchant, "delete");
});

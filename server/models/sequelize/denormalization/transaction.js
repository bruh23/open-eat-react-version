const Transactions      = require('../Transactions');
const TransactionMongo  = require('../../Transaction');

const denormalizeTransactions = async (transaction, operation) => {
    console.log('ADD TRANSACTION');

    transaction = transaction.dataValues;

    await TransactionMongo.deleteOne({ id: transaction.id });
    
    if (operation !== "delete") {
        
        transaction.merchantId = transaction.MerchantId;

        const doc = new TransactionMongo(transaction);
        await doc.save();
    }
};

console.log('Add hook Transaction');

Transactions.addHook('afterCreate', (transaction) => {
    denormalizeTransactions(transaction, 'create');
});

Transactions.addHook("afterUpdate", (transaction) => {
    denormalizeTransactions(transaction, "update");
});

Transactions.addHook("afterDestroy", (transaction) => {
    denormalizeTransactions(transaction, "delete");
});
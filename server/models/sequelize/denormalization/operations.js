const Operations = require('../Operations');
const Transaction = require("../../Transaction");

const denormalizeOperations = async (operation, action) => {
    console.log('ACTION: ', action);

    operation = operation.dataValues;

    if(action == 'create') {
        Transaction.findOne({id: operation.TransactionId})
        .then(transaction => {
            let operationArray = transaction.operations;

            operationArray.push(operation);

            Transaction.updateOne({id: operation.TransactionId}, {
                $set: {
                    operations: operationArray
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            })
        })
        .catch(error => {
            console.log(error);
        })
    }

    if(action == 'update') {
        Transaction.findOne({id: operation.TransactionId})
        .then(transaction => {
            let operationArray = transaction.operations;
            let findoperation = operationArray.findIndex(elem => elem.id === operation.id);

            operationArray.splice(findoperation, 1);
            operationArray.push(operation);

            Transaction.updateOne({id: operation.TransactionId}, {
                $set : {
                    operations: operationArray
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
        });
    }

    if(action == 'delete') {
        console.log(operation);
        
        Transaction.findOne({id: operation.TransactionId})
        .then(transaction => {
            let operationArray = transaction.operations;
            let findoperation = operationArray.findIndex(elem => elem.id === operation.id);

            operationArray.splice(findoperation, 1);

            Transaction.updateOne({id: operation.TransactionId}, {
                $set : {
                    operations: operationArray
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
        });
    }
};

console.log('Add hook User');

Operations.addHook("afterUpdate", (operation) => {
    denormalizeOperations(operation, "update");
});

Operations.addHook("afterCreate", (operation) => {
    denormalizeOperations(operation, "create");
});

Operations.addHook("afterDestroy", (operation) => {
    denormalizeOperations(operation, "delete");
});

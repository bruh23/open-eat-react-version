const Products = require('../Products');
const Merchant = require("../../Merchant");

const denormalizeProducts = async (product, operation) => {
    console.log('OPERATION', operation);

    product = product.dataValues;

    if(operation == 'create') {
        Merchant.findOne({id: product.MerchantId})
        .then(merchant => {
            let productArray = merchant.products;
    
            productArray.push(product);
    
            Merchant.updateOne({id: product.MerchantId}, {
                $set: {
                    products: productArray
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            })
        });
    }

    if(operation == 'update') {
        Merchant.findOne({id: product.MerchantId})
        .then(merchant => {
            let productArray = merchant.products;
            let findproduct = productArray.findIndex(elem => elem.id === product.id);

            productArray.splice(findproduct, 1);
            productArray.push(product);

            Merchant.updateOne({id: product.MerchantId}, {
                $set : {
                    products: productArray
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
        });
    }

    if(operation == 'delete') {
        console.log(product);
        
        Merchant.findOne({id: product.MerchantId})
        .then(merchant => {
            let productArray = merchant.products;
            let findproduct = productArray.findIndex(elem => elem.id === product.id);

            productArray.splice(findproduct, 1);

            Merchant.updateOne({id: product.MerchantId}, {
                $set : {
                    products: productArray
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
        });
    }
};

console.log('Add hook User');

Products.addHook("afterUpdate", (product) => {
    denormalizeProducts(product, "update");
});

Products.addHook("afterCreate", (product) => {
    denormalizeProducts(product, "create");
});

Products.addHook("afterDestroy", (product) => {
    denormalizeProducts(product, "delete");
});

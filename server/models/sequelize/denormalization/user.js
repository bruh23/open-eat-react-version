const UserMongo = require("../../User");
const User = require("../User");

const denormalizeUser = async (user, operation) => {
    console.log('OPERATION', operation);

    await UserMongo.deleteOne({ id: user.id });
    
    if (operation !== "delete") {
        
        user = user.dataValues;

        const doc = new UserMongo(user);
        await doc.save();
    }
};

console.log('Add hook User');

User.addHook("afterUpdate", (user) => {
    denormalizeUser(user, "update");
});

User.addHook("afterCreate", (user) => {
    denormalizeUser(user, "create");
});

User.addHook("afterDestroy", (user) => {
    denormalizeUser(user, "delete");
});

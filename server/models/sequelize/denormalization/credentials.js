const Merchant = require("../../Merchant");
const Credentials      = require('../Credentials');

const denormalizeCredentials = async (credential, operation) => {
    console.log('ADD CREDENTIALS', operation);

    credential = credential.dataValues;

    if(operation == 'create') {
        Merchant.findOne({id: credential.MerchantId})
        .then(merchant => {
            Merchant.updateOne({id: credential.MerchantId}, {
                $set: {
                    credentials: credential
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            })
        });
    }

    if(operation == 'update') {
        Merchant.findOne({id: credential.MerchantId})
        .then(merchant => {
            Merchant.updateOne({id: credential.MerchantId}, {
                $set : {
                    credentials: credential
                }
            })
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
        });
    }
};

console.log('Add hook Credentials');

Credentials.addHook('afterCreate', (credential) => {
    denormalizeCredentials(credential, 'create');
});

Credentials.addHook("afterUpdate", (credential) => {
    denormalizeCredentials(credential, "update");
});

Credentials.addHook("afterDestroy", (credential) => {
    denormalizeCredentials(credential, "delete");
});

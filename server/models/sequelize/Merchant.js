const connection = require("../../lib/sequelize");
const Sequelize = require("sequelize");
const Transactions = require('./Transactions');
const Credentials = require('./Credentials');
const bcrypt = require("bcrypt");
const Products = require("./Products");
const User = require('./User')

class Merchant extends Sequelize.Model { }

Merchant.init(
    {
        first_name: Sequelize.DataTypes.STRING,
        last_name: Sequelize.DataTypes.STRING,
        name_society: Sequelize.DataTypes.STRING,
        KBIS: Sequelize.DataTypes.STRING,
        email: Sequelize.DataTypes.STRING,
        phone_number: Sequelize.DataTypes.STRING,
        address: Sequelize.DataTypes.STRING,
        postal_code: Sequelize.DataTypes.INTEGER,
        city: Sequelize.DataTypes.STRING,
        url_confirmation: Sequelize.DataTypes.STRING,
        url_cancellation: Sequelize.DataTypes.STRING,
        currency: Sequelize.DataTypes.STRING,
        valid: Sequelize.DataTypes.BOOLEAN,
        delai_livraison: Sequelize.DataTypes.STRING
    },
    {
        sequelize: connection,
        modelName: "Merchant",
        timestamps: true,
    }
);

Merchant.hasMany(Transactions);
Merchant.hasMany(Products);
Merchant.hasOne(Credentials);

module.exports = Merchant;
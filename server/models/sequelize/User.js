const connection = require("../../lib/sequelize");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");

const Merchant = require('./Merchant')

class User extends Sequelize.Model { }

User.init(
    {
        email: Sequelize.DataTypes.STRING,
        password: {
            type: Sequelize.DataTypes.STRING,
            allowNull: false,
        },
        admin: Sequelize.DataTypes.BOOLEAN,
        confirm: Sequelize.DataTypes.BOOLEAN
    },
    {
        sequelize: connection,
        modelName: "User",
        timestamps: true,
    }
);

User.hasOne(Merchant);

User.addHook("beforeCreate", async (user) => {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
});

module.exports = User;
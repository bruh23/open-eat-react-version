const connection = require("../../lib/sequelize");
const Sequelize = require("sequelize");
const Merchant = require('./Merchant');
const bcrypt = require("bcrypt");

class Credentials extends Sequelize.Model { }

Credentials.init(
    {
        client_token : Sequelize.DataTypes.STRING,
        client_secret: Sequelize.DataTypes.STRING
    },
    {
        sequelize: connection,
        modelName: "Credentials",
        timestamps: true,
    }
);

module.exports = Credentials;
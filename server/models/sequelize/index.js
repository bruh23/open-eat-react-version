const connection = require("../../lib/sequelize");

/**
 * Denormalization
 */
require('./denormalization/user');
require('./denormalization/transaction');
require('./denormalization/merchant');
require('./denormalization/products');
require('./denormalization/operations');
require('./denormalization/credentials');

/**
 * Require models
 */
const User = require('./User');
const Transaction = require('./Transactions');
const Merchant = require('./Merchant')
const Products = require('./Products');
const Operations = require('./Operations');
const Crendetials = require('./Credentials');

/**
 * Connexion sequelize
 * 
 * force : true -> /!\ Erase all datas
 * alter : true -< /!\ Only alter columns
 */
connection.sync({
    alter: true,
    // force: true
});

/**
 * Export modules
 */
module.exports = {
    connection,
    User,
    Transaction,
    Merchant,
    Products,
    Operations,
    Crendetials
};
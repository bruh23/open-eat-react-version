const connection = require("../../lib/sequelize");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");
const { Transaction } = require("sequelize");
const Products = require("./Products");

class HasProduct extends Sequelize.Model { }

HasProduct.init(
    {
        statut: Sequelize.DataTypes.STRING,
        valid: Sequelize.DataTypes.BOOLEAN
    },
    {
        sequelize: connection,
        modelName: "HasProduct",
        timestamps: true,
    }
);

module.exports = HasProduct;
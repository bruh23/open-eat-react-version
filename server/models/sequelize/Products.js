const connection = require("../../lib/sequelize");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");

const Operations = require('./Operations'); 

class Products extends Sequelize.Model { }

Products.init(
    {
        nom_product: Sequelize.DataTypes.STRING,
        desc_product: Sequelize.DataTypes.STRING,
        image_product: Sequelize.DataTypes.STRING,
        price: Sequelize.DataTypes.STRING,
        stock: Sequelize.DataTypes.STRING
    },
    {
        sequelize: connection,
        modelName: "Products",
        timestamps: true,
    }
);

module.exports = Products;
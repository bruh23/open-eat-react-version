const connection = require("../../lib/sequelize");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");

const Operations = require('./Operations'); 

class Transactions extends Sequelize.Model { }

Transactions.init(
    {
        price: Sequelize.DataTypes.STRING,
        ended: Sequelize.DataTypes.BOOLEAN,
        acheteurINFO: Sequelize.DataTypes.JSON,
        products: Sequelize.DataTypes.JSON
    },
    {
        sequelize: connection,
        modelName: "Transactions",
        timestamps: true,
    }
);

Transactions.hasMany(Operations);

module.exports = Transactions;
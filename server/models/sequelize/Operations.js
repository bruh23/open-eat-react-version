const connection = require("../../lib/sequelize");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");

class Operations extends Sequelize.Model { }

Operations.init(
    {
        statut: Sequelize.DataTypes.STRING,
        valid: Sequelize.DataTypes.BOOLEAN,
        price: Sequelize.DataTypes.STRING
    },
    {
        sequelize: connection,
        modelName: "Operations",
        timestamps: true,
    }
);

module.exports = Operations;
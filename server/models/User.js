const mongoose = require("mongoose");
const connect = require("../lib/db");

const UserSchema = new mongoose.Schema({
    id: Number,
    email: String,
    password: String,
    createdAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    },
    admin: Boolean,
    confirm: Boolean
}, {
    collection: "User"
});

const Users = connect.model("User", UserSchema);

module.exports = Users;
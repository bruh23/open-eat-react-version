const express = require('express');
const mustacheExpress = require("mustache-express");
const verifyJwt = require("./middlewares/verifyJwt");
const cors = require("cors");

const MarchandRouter = require('./routes/merchant');
const TransictionRouter = require('./routes/transaction');
const UserRouteur = require('./routes/user');
const ProductsRouteur = require('./routes/products');
const OperationRouteur = require('./routes/operations');
const CredentialsRouteur = require('./routes/credentials');
const ApiRouteur = require('./routes/api');

const Users = require('./models/User');

const test = require('./models/sequelize/index');
const mail = require('./middlewares/mail');

const api = require('./middlewares/api');

/**
 * Init Express
 */
const app = express();

/**
 * Middlewares
 */

app.use(express.json());
app.use(express.urlencoded());
app.use(cors());


/**
 * Set Engine Mustache
 */
app.engine("mustache", mustacheExpress());
app.set("view engine", "mustache");
app.set("views", __dirname + "/views");
app.use(express.static(__dirname + '/public'));

/**
 * Routers
 */
app.use('/merchant', MarchandRouter);
app.use('/transaction', TransictionRouter);
app.use('/user', UserRouteur);
app.use('/products', ProductsRouteur);
app.use('/operations', OperationRouteur);
app.use('/credentials', CredentialsRouteur);
app.use('/api', ApiRouteur);

app.listen(3000, () => console.log("listening..."));



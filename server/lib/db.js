const mongoose = require('mongoose');

mongoose.connect(`mongodb://${process.env.MONGODB_USER}:${process.env.MONGODB_PASS}@mongo`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(
    result => console.log("mongo connected")
)
.catch(
    error => console.log(error)
);

module.exports = mongoose.connection;
// const { SakilaFilms } = require("../models");
const Router = require("express").Router;
const router = Router();

const db = require('../models/sequelize');
const Transaction = require('../models/Transaction');

const verifyJwt = require("../middlewares/verifyJwt");

/**
 * Obtenir toutes les transactions
 */
router.get('/', verifyJwt, (req, res) => {
    Transaction.find()
    .then(transactions => {
        return res.json(transactions);
    })
    .catch(error => {
        return res.sendStatus(500);
    })
});

/**
 * Obtenir une transaction
 */
router.get('/:idTransaction', verifyJwt, (req, res) => {
    Transaction.findOne({id: req.params.idTransaction})
    .then(result => {
        res.json(result);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Obtenir les transactions d'un marchand
 */
router.get('/merchant/:idMerchant', verifyJwt, (req, res) => {
    Transaction.find({merchantId: req.params.idMerchant})
    .then(results => {
        res.json(results);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Modifier une transaction
 */
router.patch('/:idTransaction', verifyJwt, (req, res) => {
    db.Transaction.update(req.body, {individualHooks: true, where: {id: req.params.idTransaction}})
    .then(update => {
        res.json(update);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

router.delete('/:idTransaction', verifyJwt, (req, res) => {
    db.Transaction.destroy({individualHooks: true, where: {id: req.params.idTransaction}})
    .then(destroy => {
        res.json(destroy)
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Ajouter une transaction
 */
router.put('/:idMerchant', (req, res) => {

    req.body.MerchantId = req.params.idMerchant;

    db.Transaction.create(req.body)
    .then(create => {
        res.json(create);
    })
    .catch(error => {
        res.sendStatus(500);
    });
});

/**
 * Page
 */
router.get('/show/:idTransaction', (req, res) => {
    Transaction.findOne({id: req.params.idTransaction})
    .then(transaction => {
        let total = 0;

        for(let i = 0; i < transaction.products.length; i++) {
            total = total + parseInt(transaction.products[i].quantity);
        }

        let find = transaction.operations.find(elem => elem.statut === "En attente de paiement");

        if(find === undefined) {
            let operation = {
                statut: "En attente de paiement",
                valid: 0,
                price: 0,
                TransactionId: req.params.idTransaction
            };
    
            /**
             * Create Operation
             */
            db.Operations.create(operation)
            .catch(error => {
                console.log(error);
                return res.sendStatus(500);
            });
        }

        res.render('transaction', {
            transaction: transaction,
            total: total
        });
        
    })
    .catch(error => {
        res.status(500).json({'error': 'Something went wrong, retry later or contact an administrator.'})
    })
});

/**
 * Post Paiement
 */
router.post('/show/:idTransaction', (req, res) => {
    let acheteurInfo = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        address: req.body.address,
        address2: req.body.address2,
        country: req.body.country,
        state: req.body.state,
        zipcode: req.body.zipcode
    };

    Transaction.findOne({id: req.params.idTransaction})
    .then(transaction => {

        let addTransaction = {
            price: transaction.price,
            ended: transaction.ended,
            acheteurINFO: acheteurInfo,
            products: transaction.products,
            MerchantId: transaction.merchantId
        };

        /**
         * Update Transaction
         */
        db.Transaction.update(addTransaction, {individualHooks: true, where: {id: req.params.idTransaction}})
        .then(updated => {
            // continue
            let infoBanque = {
                displayed_name: req.body.displayed_name,
                expiration: req.body.expiration,
                cvv: req.body.cvv,
                credit_card: req.body.credit_card
            };

            return res.render('psp', {
                transaction: transaction,
                infoBanque: infoBanque
            });
        })
        .catch(error => {
            console.log(error);
            return res.sendStatus(500);
        })
        
    })
    .catch(error => {
        console.log(error);
        return res.sendStatus(500);
    })
});


/**
 * Add product(s) to transaction
 */
router.put('/products/:idTransaction/:idProduct/:quantity', (req, res) => {
    db.Products.findOne({where: {id: req.params.idProduct}})
    .then(product => {
        product = product.dataValues;
        product.quantity = req.params.quantity;

        db.Transaction.findOne({where: {id: req.params.idTransaction}})
        .then(transaction => {
            transaction = transaction.dataValues;

            let price = parseFloat(transaction.price) + (parseFloat(product.price) *  parseInt(req.params.quantity));

            transaction.price = price;

            let findProduct = transaction.products.findIndex(elem => elem.id === parseInt(req.params.idProduct));

            console.log(findProduct);

            if(findProduct !== -1) {
                transaction.products[findProduct].quantity = parseInt(transaction.products[findProduct].quantity) + parseInt(req.params.quantity);
            } else {
                transaction.products.push(product);
            }

            db.Transaction.update(transaction, {where: {id: req.params.idTransaction}})
            .then(updated => {
                Transaction.updateOne({id: transaction.id}, {
                    $set: {
                        price: price,
                        products: transaction.products
                    }
                })
                .then(response => {
                    return res.json(response);
                })
                .catch(error => {
                    console.log(error);
                    return res.sendStatus(500);
                })
            })
            .catch(error => {
                console.log(error);
                return res.sendStatus(500);
            })
        })
        .catch(error => {
            console.log(error);
            return res.sendStatus(500);
        });
    })
    .catch(error => {
        console.log(error);
        return res.sendStatus(500);
    })
});

/**
 * Remove product(s) to transaction
 */
router.delete('/products/:idTransaction/:idProduct/:quantity', (req, res) => {
    db.Transaction.findOne({where: {id: req.params.idTransaction}})
    .then(transaction => {
        transaction = transaction.dataValues;

        let products = transaction.products;
        let find = products.findIndex(elem => elem.id === parseInt(req.params.idProduct));

        if(find !== -1) {
            let price;

            if(req.params.quantity == products[find].quantity) {
                price = parseFloat(transaction.price) - parseFloat(products[find].price);
                products.splice(find, 1);
            } else {
                products[find].quantity = parseInt(products[find].quantity) - parseInt(req.params.quantity);
                price = transaction.price - (parseFloat(products[find].price) * parseInt(req.params.quantity));
            }

            if(price < 0) {
                price = 0;
            }

            transaction.price = price;

            db.Transaction.update(transaction, {where: {id: req.params.idTransaction}})
            .then(updated => {
                console.log(updated);

                Transaction.updateOne({id: transaction.id}, {
                    $set: {
                        price: price,
                        products: products
                    }
                })
                .then(response => {
                    return res.json(response);
                })
                .catch(error => {
                    console.log(error);
                    return res.sendStatus(500);
                })
            })
            .catch(error => {
                console.log(error);
                return res.sendStatus(500);
            })
        } else {
            return res.sendStatus(500);
        }
    })
    .catch(error => {
        console.log(error);
        return res.sendStatus(500);
    })
});

module.exports = router;
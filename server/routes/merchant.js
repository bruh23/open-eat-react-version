// const { SakilaFilms } = require("../models");
const Router = require("express").Router;
const router = Router();

const db = require('../models/sequelize');
const Merchant = require('../models/Merchant');

const verifyJwt = require("../middlewares/verifyJwt");

const { v4: uuidv4 } = require('uuid');

/**
 * Lister tous les marchands
 */
router.get('/', verifyJwt, (req, res) => {
    Merchant.find()
    .then(merchants => {
        res.json(merchants);
    })
    .catch(error => {
        res.sendStatus(500);
    });
});

/**
 * Nouveau Marchand
 */
router.put('/:idUser', (req, res) => {

    if(req.body.UserId) {
        req.body.UserId = null;
    }

    req.body.UserId = req.params.idUser;

    db.Merchant.create(req.body)
    .then(user => {
        
        let body = {
            client_token : uuidv4(),
            client_secret : uuidv4(),
            MerchantId: user.dataValues.id
        };
    
        db.Crendetials.create(body)
        .then(credentials => {
            console.log(credentials);
        })
        .catch(error => {
            console.log(error);
        })

        res.json(user);
    })
    .catch(error => {
        console.log(error);
        res.sendStatus(500);
    });
});

/**
 * Obtenir un marchand par id
 */
router.get('/:id', verifyJwt, (req, res) => {
    Merchant.findOne({id: req.params.id})
    .then(merchant => {
        res.json(merchant);
    })
    .catch(error => {
        res.sendStatus(500);
    });
});

/**
 * Supprimer un marchand
 */
router.delete('/:id', verifyJwt, (req, res) => {
    db.Merchant.destroy({individualHooks: true,where: {id: req.params.id}})
    .then(merchant => {
        res.sendStatus(200);
    })
    .catch(error => {
        res.sendStatus(500);
    });
});

/**
 * Editer un marchand
 */
router.patch('/:id', verifyJwt, (req, res) => {
    db.Merchant.update(req.body, {individualHooks: true, where: {id: req.params.id}})
    .then(update => {
        res.json(update);
    })
    .catch(error => {
        res.sendStatus(500);
    })
})

module.exports = router;
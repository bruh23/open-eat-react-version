const Router = require("express").Router;
const router = Router();

const db = require('../models/sequelize');
const Merchant = require('../models/Merchant');

const verifyJwt = require("../middlewares/verifyJwt");

/**
 * Obtenir un produit
 */
router.get('/:idProduct', verifyJwt, (req, res) => {
    db.Products.findOne({where: {id: req.params.idProduct}})
    .then(product => {
        res.json(product);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Obtenir les products d'un marchand
 */
router.get('/merchant/:idMerchant', (req, res) => {
    Merchant.findOne({id: req.params.idMerchant})
    .then(merchant => {
        res.json(merchant.products);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Modifier une product
 */
router.patch('/:idProduct', verifyJwt, (req, res) => {
    db.Products.update(req.body, {individualHooks: true, where: {id: req.params.idProduct}})
    .then(update => {
        res.json(update);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Supprimer un produit
 */
router.delete('/:idProduct', verifyJwt, (req, res) => {
    db.Products.destroy({individualHooks: true, where: {id: req.params.idProduct}})
    .then(destroy => {
        res.json(destroy)
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Ajouter une product
 */
router.put('/:idMerchant', verifyJwt, (req, res) => {

    console.log(req.body);

    req.body.MerchantId = req.params.idMerchant;

    db.Products.create(req.body)
    .then(create => {
        res.json(create);
    })
    .catch(error => {
        res.sendStatus(500);
    });

});

module.exports = router;
const Router = require("express").Router;
const router = Router();

const db = require("../models/sequelize");
const Transaction = require("../models/Transaction");

const verifyJwt = require("../middlewares/verifyJwt");

/**
 * Obtenir un produit
 */
router.get("/:idTransaction", verifyJwt, (req, res) => {
  db.Operations.findOne({ where: { id: req.params.idTransaction } })
    .then((operation) => {
      res.json(operation);
    })
    .catch((error) => {
      res.sendStatus(500);
    });
});

/**
 * Obtenir les operations d'un marchand
 */
router.get("/transaction/:idTransaction", verifyJwt, (req, res) => {
  Transaction.findOne({ id: req.params.idTransaction })
    .then((transaction) => {
      res.json(transaction.operations);
    })
    .catch((error) => {
      res.sendStatus(500);
    });
});

/**
 * Modifier une operation
 */
router.patch("/:idTransaction", verifyJwt, (req, res) => {
  db.Operations.update(req.body, {
    individualHooks: true,
    where: { id: req.params.idTransaction },
  })
    .then((update) => {
      res.json(update);
    })
    .catch((error) => {
      res.sendStatus(500);
    });
});

/**
 * Supprimer un produit
 */
router.delete("/:idTransaction", verifyJwt, (req, res) => {
  db.Operations.destroy({
    individualHooks: true,
    where: { id: req.params.idTransaction },
  })
    .then((destroy) => {
      res.json(destroy);
    })
    .catch((error) => {
      res.sendStatus(500);
    });
});

/**
 * Ajouter une operation
 */
router.put("/:idTransaction", (req, res) => {
  req.body.TransactionId = req.params.idTransaction;

  db.Operations.create(req.body)
    .then((create) => {
      res.json(create);
    })
    .catch((error) => {
      res.sendStatus(500);
    });
});

module.exports = router;

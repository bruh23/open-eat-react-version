const Router = require("express").Router;
const router = Router();

const { v4: uuidv4 } = require('uuid');

const db = require('../models/sequelize');
const Merchant = require('../models/Merchant');

const verifyJwt = require("../middlewares/verifyJwt");

/**
 * Get credentials
 */
router.get('/:idMerchant', verifyJwt, (req, res) => {
    Merchant.findOne({id: req.params.idMerchant})
    .then(merchant => {
        res.json(merchant.credentials);
    })
    .catch(error => {
        res.sendStatus(500);
    });
    
});

/**
 * Generate new one
 */
router.patch('/:idMerchant', verifyJwt, (req, res) => {
    let body = {
        client_token : uuidv4(),
        client_secret : uuidv4()
    };
    
    db.Crendetials.update(body, {individualHooks: true, where: {MerchantId: req.params.idMerchant}})
    .then(update => {
        res.json(update);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Generate credentials for an merchant
 */
router.put('/:idMerchant', (req, res) => {
    let body = {
        client_token : uuidv4(),
        client_secret : uuidv4(),
        MerchantId: req.params.idMerchant
    };

    db.Crendetials.create(body)
    .then(credentials => {
        res.json(credentials);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

module.exports = router;
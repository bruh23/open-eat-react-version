const Router = require("express").Router;
const router = Router();

const { v4: uuidv4 } = require('uuid');

const db = require('../models/sequelize');
const Transaction = require('../models/Transaction');

const verifyToken = require("../middlewares/verifyToken");

/**
 * Créer une transaction
 */
router.put('/transactions', verifyToken, (req, res) => {
    let user = res.user;

    req.body.MerchantId = user.id;

    db.Transaction.create(req.body)
    .then(result => {
        res.json(result);
    })
    .catch(error => {
        res.sendStatus(500);
    });
})

/**
 * Lister ses transactions
 */
router.get('/transactions', verifyToken, (req, res) => {
    let user = res.user;

    Transaction.find({merchantId: user.id})
    .then(transactions => {
        res.json(transactions);
    })
    .catch(error => {
        res.sendStatus(500);
    });
});

/**
 * Afficher une transaction
 */
router.get('/transactions/:idTransaction', verifyToken, (req, res) => {
    Transaction.findOne({id: req.params.idTransaction})
    .then(transaction => {
        console.log(transaction);
        res.json(transaction);
    })
    .catch(error => {
        console.log(error);
        res.sendStatus(500);
    })
});

/**
 * Lister les opérations d'une transactions
 */
router.get('/transactions/operations/:idTransaction', verifyToken, (req, res) => {
    let user = res.user;

    Transaction.findOne({merchantId: user.id, id: req.params.idTransaction})
    .then(transaction => {
        res.json(transaction.operations);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Afficher une opération
 */
router.get('/operations/:idTransaction/:idOperation', verifyToken, (req, res) => {
    let user = res.user;

    Transaction.findOne({merchantId: user.id, id: req.params.idTransaction})
    .then(transaction => {

        let findTransaction = transaction.operations.find(elem => elem.id === parseInt(req.params.idOperation));

        if(findTransaction === undefined) {
            res.status(500).json({"error": "Nothing found"});
        }

        res.json(findTransaction);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Créer une opération
 */
router.put('/operations/:idTransaction', verifyToken, (req, res) => {
    req.body.TransactionId = req.params.idTransaction;

    db.Operations.create(req.body)
    .then(result => {
        res.json(result);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

module.exports = router;
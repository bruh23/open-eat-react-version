const Router = require("express").Router;
const router = Router();

const {createToken, verifyToken}  = require('../lib/jwt');
const bcrypt = require("bcrypt");

const { v4: uuidv4 } = require('uuid');

const db = require('../models/sequelize');

const User = require('../models/User');
const Merchant = require('../models/Merchant');
const ConfirmUser = require('../models/ConfirmUser')

const verifyJwt = require("../middlewares/verifyJwt");
const sendMail  = require('../middlewares/mail');

/**
 * Login
 */
router.put('/login', async (req, res) => {
    if(req.body) {
        if(!req.body.email || !req.body.password) {
            res.sendStatus(500);
        }

        db.User.findOne({where: {email: req.body.email}})
        .then(async (user) => {
            /**
             * If Email incorrect, send error
             */
            if(user === null) {
                return res.json({'error': 'Incorrect Credentials'});
            }

            user = user.dataValues;

            /**
             * Comparate plain text password with hash in db (pgsql)
             */
            bcrypt.compare(req.body.password, user.password, async (err, result) => {
                if(!result) {
                    return res.json({'error': 'Incorrect Credentials'});
                } else {
                    let token = await createToken(user);
                    
                    return res.json({'valid': true, token: token});
                }
            });
        })
        .catch(error => {
            res.sendStatus(500);
        });
    } else {
        res.sendStatus(500);
    }
});

/**
 * Get Marchant
 */
router.get('/merchant/:idUser', (req, res) => {
    Merchant.findOne({userId: req.params.idUser})
    .then(merchant => {
        console.log(merchant);
        res.json(merchant);
    })
    .catch(error => {
        console.log(error);
        res.sendStatus(500);
    });
});

/**
 * Validate User
 */
router.get('/validation/:token', (req, res) => {
    ConfirmUser.findOne({token: req.params.token})
    .then(confirm => {
        let valid = {
            confirm: true
        };
    
        db.User.update(valid, {individualHooks: true, where: {id: confirm.userId}})
        .then(update => {

            ConfirmUser.deleteOne({token : req.params.token})
            .then(deleted => {
                console.log(deleted);
            })
            .catch(error => {
                console.log(error);
            })

            res.sendStatus(200);
        })
        .catch(error => {
            res.sendStatus(500);
        })
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Add User
 */
router.put('/', (req, res) => {
    db.User.create(req.body)
    .then(user => {

        var today = new Date();
        var tomorrow = new Date();

        tomorrow.setDate(today.getDate()+1);
        
        let randomUuid = uuidv4();

        let format = {
            token: randomUuid,
            expireDate: tomorrow.getTime(),
            userId: user.id
        };

        ConfirmUser.create(format)
        .then(confirm => {
            console.log(confirm);
        })
        .catch(error => {
            console.log(error);
        });
        
        sendMail(user.email, 'Validez votre compte', 'Lien de validation: http://localhost:3003/user/validation/' + randomUuid);
        res.json(user);
    })
    .catch(error => {
        res.sendStatus(500);
    });
});

/**
 * Get all users
 */
router.get('/', verifyJwt, (req, res) => {
    User.find()
    .then(users => {
        res.json(users);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Get User by Id
 */
router.get('/:id', verifyJwt, (req, res) => {
    User.findOne({id: req.params.id})
    .then(user => {
        res.json(user);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Supprimer un Utilisateur
 */
router.delete('/:id', verifyJwt, (req, res) => {
    db.User.destroy({individualHooks: true,where: {id: req.params.id}})
    .then(deleted => {
        console.log(deleted);
        res.sendStatus(200);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

/**
 * Update User
 */
router.patch('/:id', (req, res) => {
    db.User.update(req.body, {individualHooks: true, where: {id: req.params.id}})
    .then(update => {
        res.json(update);
    })
    .catch(error => {
        res.sendStatus(500);
    })
});

module.exports = router;
const http = require('http');
const https = require('https');

const getCurrency = async () => {
    return new Promise((resolve, reject) => {
        https.get('https://api.exchangeratesapi.io/latest?base=EUR', (resp) => {
            let data = '';
            
            resp.on('data', (chunk) => {
                data += chunk;
            });
            
            resp.on('end', () => {
                return resolve(data);
            });
    
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    });
};

module.exports = getCurrency;
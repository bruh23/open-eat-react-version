const Merchant = require('../models/Merchant');

const verifyToken = (req, res, next) => {
    let headers = req.headers;

    if(!headers.authorization) {
        return res.status(500).json({'error': 'Headers not correctly formatted'});
    }
    
    let authorization = headers.authorization + ' ';

    let regexToken  = /(?<=client_token=)(.*?)(?= )/;
    let regexSecret = /(?<=client_secret=)(.*?)(?= )/;
    
    if(authorization.match(regexToken) === null || authorization.match(regexSecret) === null) {
        return res.status(500).json({'error': 'Please, provide client_token & client_secret'});
    }

    let client_token = authorization.match(regexToken)[0];
    let client_secret = authorization.match(regexSecret)[0];

    Merchant.findOne({"credentials.client_token": client_token, "credentials.client_secret": client_secret})
    .then(merchant => {
        res.user = merchant;
        next()
    })
    .catch(error => {
        console.log(error);
        return res.sendStatus(500);
    });

}

module.exports = verifyToken;